angular.module('app')
       .service('CONFIG', function () {
         'use strict';
         var getApiAddress = function () {
           var test    = false;
           var h       = !test ? window.location.hostname : 'localMachineTest';
           var apiHost = null;
    
           switch (h) {
             case 'localhost':
               apiHost = 'http://adminapi.oddscombiner.com/';
               break;
             case 'localMachineTest':
               apiHost = 'http://localhost:50012/';
               break;
             case 'admin.oddscombiner.com':
               apiHost = 'http://adminapi.oddscombiner.com/';
               break;
             case 'devadmin.oddscombiner.com':
               apiHost = 'http://adminapi.oddscombiner.com/';
               break;
           }
           return apiHost;
         };
  
  
         this.api = {
           url    : getApiAddress(),
           version: 'api/'
         };
  
         this.encryption = require('./encryption.json');
         this.session    = require('./session.json');
       });
